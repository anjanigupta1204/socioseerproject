/**
 * http://usejsdoc.org/
 */
"use strict";
var express = require('express');
var async = require("async");
var router = express.Router();
var path = require('path');
var successCode = 'success';
var errorCode = 'error';
var failureCode = 'failure';
var http = require('http');
var request = require('request'); 
var fs = require('fs');
var querystring = require('querystring');
var _host = '192.168.1.16';
var _port = '8083';

function modifyString(inputString, replacingChar, replaceWith) {
	var str = inputString.split(replacingChar).join(replaceWith);
	return str;
}

function getFormData(dataKey, fileKey, data, file) { 
		
		switch (dataKey) {
		case 'user':
			var formData = {
				//logo: fs.createReadStream(__dirname+'/uploads/file.jpg'),
				logo: file,
				user: JSON.stringify(data)
			};
		break;

		case 'client':
			delete data.startDateString;
			delete data.endDateString;
			var formData = {
				logo: file,
				client: JSON.stringify(data)
			};
			break; 
			
		default:
			break;
		}
		console.log('formData@@@@@@',formData,'^^^^^');
		return formData;
 
}

router.route('')
	.post(function (req, res) {
		//var dataArray = '{"user":{}}';
		/*dataArray = JSON.stringify(dataArray);*/
		//  dataArray = dataArray.trim();
		var dataArray = JSON.stringify(req.body.myparam);
		console.log('data Array: ', dataArray);
		var action = modifyString(req.body.action, '_', '/');

		if (req.headers['x-auth-header'] && req.headers['x-auth-header'].length) {
			var _headers = {
				'Content-Type': 'application/json',
				//'X-AUTH-HEADER': req.headers['x-auth-header'],
				'X-AUTH-HEADER': 'v3Ry5ecR3tK3Y',
				'Content-Length': Buffer.byteLength(dataArray),
			}
		} else {
			var _headers = {
				'Content-Type': 'application/json',
				'X-AUTH-HEADER': 'v3Ry5ecR3tK3Y',
				'Content-Length': Buffer.byteLength(dataArray),
			}
		}



		var options = {
			host: _host,
			port: _port,
			path: '/' + action,
			method: 'POST',
			headers: _headers

		};
		var str = '';
		var httpreq = http.request(options, function (response) {
			response.setEncoding('utf8');
			response.on('data', function (chunk) {
				//res.send(JSON.stringify(chunk));
				str += chunk;
			});
			response.on('end', function () {
				res.send(str);
				//res.send(JSON.stringify(data));
			})
		});

		httpreq.on('error', (e) => {
			console.log(`problem with request: ${e.message}`);
		});
		httpreq.write(dataArray);
		httpreq.end();

	});

router.route('/media')
	.post(function (req, res) {
		var data = req.body.rawBody;
		var dataKey = req.body.rawBodyKey;
		var file = req.body.file;
		var fileKey = req.body.fileKey;
		var action = modifyString(req.body.action, '_', '/');
		var opts = {
			url: 'http://' + _host + ':' + _port + '/' + action,
			method: 'POST',
			headers: {
				'X-AUTH-HEADER': 'v3Ry5ecR3tK3Y',
			},

			json: true,
			formData: getFormData(dataKey, fileKey, data, file)

		}
		var str = '';
		
		request(opts, function (err, resp, body) {
			console.log('bbbb',err, body);
			if (!err) {
				resp.setEncoding('utf8');
				resp.on('data', function (chunk) { 
				str += chunk;
			});
			res.send(body);

			}
		});
	})

router.route('')
	.get(function (req, res) {

		var dataArray = JSON.stringify(req.query);
		var dataArrayObj = req.query;

		var action = modifyString(req.query.action, '_', '/');
		var queryParam = "";
		var i = 0;
		for (var key in dataArrayObj) {
			var q = "";
			i++;
			if (dataArrayObj.hasOwnProperty(key)) {
				if (Object.keys(dataArrayObj).length == i) {
					q = key + "=" + dataArrayObj[key] + "";
					queryParam += q;
				} else {
					if (key != 'action') {
						q = key + "=" + dataArrayObj[key] + "&";
						queryParam += q;
					}

				}
			}

		}

		var urlToHit = action;
		if (queryParam.length) {
			urlToHit += '?' + queryParam;
		}

		if (req.headers['x-auth-header'] && req.headers['x-auth-header'].length) {
			var _headers = {
				'Content-Type': 'application/json',
				'X-AUTH-HEADER': 'v3Ry5ecR3tK3Y',
				//'X-AUTH-HEADER': req.headers['x-auth-header'],
				'Content-Length': Buffer.byteLength(dataArray)
			}
		} else {
			var _headers = {
				'Content-Type': 'application/json',
				'X-AUTH-HEADER': 'v3Ry5ecR3tK3Y',
				'Content-Length': Buffer.byteLength(dataArray)
			}
		}
		var options = {
			// host: '52.33.3.82',
			host: _host,
			//port: '8080',
			port: _port,
			path: '/' + urlToHit,
			method: 'GET',
			headers: _headers
		};
		var str = '';
		var httpreq = http.request(options, function (response) {
			response.setEncoding('utf8');
			response.on('data', function (chunk) {
				str += chunk;
			});
			response.on('end', function () {
				res.send(str);
			})
		});
		httpreq.on('error', (e) => {
			console.log(`problem with request: ${e.message}`);
		});

		httpreq.write(dataArray);
		httpreq.end();

	});

module.exports = router;
