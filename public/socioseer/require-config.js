'use strict'; 
require.config({
	urlArgs: "bust=" + (new Date()).getTime(),
	paths: {
		jquery: 'assets/js/lib/jquery-2.1.1',
		angular: 'assets/js/lib/angular.min',
		angularRoute: 'assets/js/lib/angular-ui-router',
		constant:'assets/js/constant',
		helper: 'assets/js/helper',
		storage: 'assets/js/storage',
		app: 'assets/js/app',
		bootstrap: 'assets/js/lib/bootstrap.min',
		mentis: 'assets/js/lib/jquery.metisMenu',
		pace: 'assets/js/lib/pace.min',
		selectize: 'assets/js/lib/selectize',
		datepicker: 'assets/js/angular-datepicker',
		script: 'assets/js/script',
	},
	shim: {
		'angular' : {'exports' : 'angular'},
		'angularRoute': {
			deps:['angular'], 
		},
		'bootstrap':{
			deps:['jquery'], 	
		},
		'mentis':{
			deps:['jquery'], 	
		},
		'pace':{
			deps:['jquery'], 	
		},
		'datepicker':{
			deps:['angular'], 	
		},'script':{
			deps:['jquery'], 	
		}
	}, 
	priority: [
		'jquery',
		'angular',
		'bootstrap',
		'mentis',
		'pace',
		'constant',
		'app',
		'helper',
		'storage',
		'script',
		'datepicker'
	]
	
});

require([
	'angular',
	'app'  
	], function(angular, app) {
		var $html = angular.element(document.getElementsByTagName('html')[0]);
		angular.element().ready(function() { 
			angular.bootstrap(document, ['socioApp']);
		});
	}
);