'use strict';
define([
	'angular',
	'angularRoute'
], function (angular) {
	angular.module('socioApp.user', ['ui.router'])
		.config(['$stateProvider', function ($stateProvider) {
			$stateProvider.state('login', {
				url: '/login',
				templateUrl: 'modules/user/views/login.html',
				controller: 'loginController'
			}).state("addUser", {
				url: '/addUser',
				templateUrl: 'modules/user/views/addUser.html',
				controller: 'addUserController',
			}).state("createRole", {
				url: '/createRole',
				templateUrl: 'modules/user/views/createRole.html',
				controller: 'createRoleController',
			}).state("editRole", {
				url: '/editRole',
				templateUrl: 'modules/user/views/editRole.html',
				controller: 'editRoleController',
			}).state("createTeam", {
				url: '/createTeam',
				templateUrl: 'modules/user/views/createTeam.html',
				controller: 'createTeamController',
			}).state("roleListing", {
				url: '/roleListing',
				templateUrl: 'modules/user/views/roleListing.html',
				controller: 'roleListingController',
			}).state("viewRoles", {
				url: '/viewRoles',
				templateUrl: 'modules/user/views/viewRoles.html',
				controller: 'viewRoleController',
			}).state("teamListing", {
				url: '/teamListing',
				templateUrl: 'modules/user/views/teamListing.html',
				controller: 'teamListingController',
			}).state("userListing", {
				url: '/userListing',
				templateUrl: 'modules/user/views/userListing.html',
				controller: 'userListingController',
			}).state("viewTeam", {
				url: '/viewTeam',
				params: {
					teamData: null
				},
				templateUrl: 'modules/user/views/viewTeam.html',
				controller: 'viewTeamController',
			})
		}
		])
		.controller('loginController', function ($scope, $location, localstorage, helperFactory, httpServices, $state) {
			//$scope.email = "admin@gmail.com";
			$scope.email = "akhil1@gmail.com";
			$scope.password = "password";
			$scope.isChecked = false;
			jQuery('body').addClass('full-width');
			var url = 'summary';
			if (helperFactory.isLoggedIn()) {
				$state.go(url);
				return false;
			}
			$scope.$emit('pageTitle', "Login");
			$scope.$emit('getHeader', false);
			$scope.$emit('getSidebar', false);
			$scope.$emit('getFooter', true);
			$scope.$emit('titleSection', false);

			$scope.rememberMe = function (isChecked) {
				console.log(isChecked);
				$scope.isChecked = isChecked;
			}

			if (localstorage.get('rememberMe') == 'true') {
				$scope.email = localstorage.get('LoginUser');
				$scope.password = localstorage.get('password');
				$scope.isChecked = localstorage.get('rememberMe');
				$scope.remember = true;
			}

			$scope.validateUser = function (email, password) {

				if (typeof email != 'undefined' && typeof password != 'undefined') {
					var formData = { 'email': email, 'password': password };
					var userData = { 'action': 'api_auth', 'myparam': formData };
					console.log(userData)
					httpServices.postRequest(API_URL, userData).then(function (data) {
						console.log('data : ', data);
						if (data.status == 200) {
							$scope.errorMsgLogin = '';
							localstorage.set('logged_in', true);
							localstorage.set('LoginUser', email);
							localstorage.set('authToken', data.data.authToken);
							localstorage.set('userId', data.data.userId);
							localstorage.set('rememberMe', $scope.isChecked);
							$state.go(url);
						} else {
							//show error msg
							$scope.errorMsgLogin = 'Invalid credentials.';
						}
					}).catch(function (error) {
						$scope.errorMsgLogin = 'Server error.';
					});
				} else {

					$scope.errorMsgLogin = 'Invalid credentials.';
				}
			}
		})
		.controller('teamListingController', function ($rootScope, $scope, $location, localstorage, helperFactory, $state, httpServices) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			$scope.$emit('pageTitle', "Team Listing");
			$scope.send = function (newValue) {
				console.log('New name!', newValue);
				localstorage.set('ctData', newValue);
				sessionStorage.currentTeamID = newValue;
				console.log('New session team id!', sessionStorage.currentTeamID);
			}
			//var action = API_PATH + "team_client_58dc93d67fc7b30d98e4d271";
			var action = API_PATH + "team_client_58e86417053e6839d40a26d1";
			var header = localstorage.get('authToken');
			//console.log(header)	
			//var url = API_URL+"?action="+action+"&pageNo=0&pageSize=10&sortingOrder=ASC&fieldName=bajaj";
			//var myparam = {'pageNo':0,'pageSize':10,'sortingOrder':'ASC','fieldName':'bajaj'};
			var params = { 'action': action, 'pageNo': 0, 'pageSize': 10, 'sortingOrder': 'ASC' };
			httpServices.getRequest(API_URL, header, params).then(function (data) {
				console.log(data);
				if (data.status == 200 && data.data.length > 0) {
					$scope.teamListData = data.data;
				} else {
					//show error msg
					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});

		})
		.controller('viewTeamController', function ($scope, $location, localstorage, helperFactory, $state, httpServices, $stateParams, $rootScope) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			console.log('team controller New session team id!', sessionStorage.currentTeamID);
			if (typeof sessionStorage.currentTeamID == 'undefined' && sessionStorage.currentTeamID == '') {
				$state.go('teamListing');
				return false;
			}
			console.log('team ind from localstorage data ', localstorage.get('ctData'));

			console.log('team ind from sessionstorage data ', sessionStorage.currentTeamID);
			$scope.$emit('pageTitle', "Team Listing");
			var action = API_PATH + "team_" + sessionStorage.currentTeamID;
			var header = localstorage.get('authToken');
			//console.log(header)
			//var url = API_URL+"?action="+action+"&pageNo=0&pageSize=10&sortingOrder=ASC&fieldName=bajaj";
			//var myparam = {'pageNo':0,'pageSize':10,'sortingOrder':'ASC','fieldName':'bajaj'};
			var params = { 'action': action, 'pageNo': 0, 'pageSize': 10, 'sortingOrder': 'ASC', 'fieldName': 'bajaj' };
			httpServices.getRequest(API_URL, header, params).then(function (data) {
				console.log('data part: ', data.data);
				if (data.status == 200) {
					$scope.teamData = data.data;
					console.log('teamData: ', $scope.teamData);
				} else {
					//show error msg
					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});

		})
		.controller('roleListingController', function ($scope, $location, localstorage, helperFactory, $state, httpServices) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			$scope.$emit('pageTitle', "Role Listing");

			$scope.createRoleData = function (selectedRoleValue) {
				console.log('New name!', selectedRoleValue);
				sessionStorage.currentRoleValue = JSON.stringify(selectedRoleValue);
				console.log('New session team id!', sessionStorage.currentRoleValue);
			}

			//var action = API_PATH + "role_all";
			var action = API_PATH + "security-group_fetchall";
			var header = localstorage.get('authToken');
			
			console.log(action);
			//var url = API_URL+"?action="+action+"&pageNo=0&pageSize=10&sortingOrder=ASC&fieldName=bajaj";
			//var myparam = {'pageNo':0,'pageSize':10,'sortingOrder':'ASC','fieldName':'bajaj'};
			var params = { 'action': action };
			httpServices.getRequest(API_URL, header, params).then(function (data) {
				console.log('------' , data);
				if (data.status == 200 && data.data.length > 0) {
					$scope.viewRoleData = data.data;
				} else {
					//show error msg
					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});

		})
		.controller('createRoleController', function ($scope, $location, localstorage, helperFactory, $state, httpServices) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			$scope.$emit('pageTitle', "Create Role");
			//var action = API_PATH + "user_client_58dc93d67fc7b30d98e4d271";
			var action = API_PATH + "roles";

			var header = localstorage.get('authToken');


			var params = { 'action': action };
			$scope.selectAll = [];

			$scope.existRoleData = null;
			httpServices.getRequest(API_URL, header, params).then(function (data) {
				console.log(data);
				if (data.status == 200 && data.data.length > 0) {
					$scope.existRoleData = data.data;
					for (var i = 0; i < data.data.length; i++) {
						$scope.rolePermissions = [{
							name: "POST"
						}, {
							name: "GET"
						}, {
							name: "PUT"
						}, {
							name: "DELETE"
						}];
						var c = 0;
						for (var j = 0; j < $scope.rolePermissions.length; j++) {
							var permsn = $scope.existRoleData[i].permissions;
							//console.log(permsn);

							if (permsn.indexOf($scope.rolePermissions[j].name) != -1) {
								$scope.rolePermissions[j].status = 1;
								c++;
							} else {
								$scope.rolePermissions[j].status = 0;
							}

						}

						if (c == $scope.rolePermissions.length) {
							$scope.existRoleData[i].checkAll = 1;
						} else {
							$scope.existRoleData[i].checkAll = 0;
						}

						$scope.existRoleData[i].role_permissions = $scope.rolePermissions;
					}
				} else {
					//show error msg
					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});

			$scope.checkAll = function (data, flag, key) {
				//console.log(typeof flag)
				//console.log($scope.existRoleData)

				if ($scope.existRoleData != null) {
					for (var i = 0; i < $scope.existRoleData.length; i++) {
						if (data == $scope.existRoleData[i].id) {
							//console.log($scope.existRoleData[i])
							if (flag) {
								angular.forEach($scope.existRoleData[i].role_permissions, function (item) {
									item.status = 1;
								});
								$scope.existRoleData[i].checkAll = 1;
							} else {
								angular.forEach($scope.existRoleData[i].role_permissions, function (item) {
									item.status = 0;
								});
								$scope.existRoleData[i].checkAll = 0;
							}
						}
					}
				}
			}

			$scope.checkIndCheck = function (key, flag, indItem) {
				//console.log(flag, indItem);
				for (var i = 0; i < $scope.existRoleData.length; i++) {

					if (key == $scope.existRoleData[i].id) {

						if (!flag) {
							angular.forEach($scope.existRoleData[i].role_permissions, function (item) {
								if (item.name == indItem) {
									item.status = 0;
								}
							});
							$scope.existRoleData[i].checkAll = 0;
							//console.log($scope.existRoleData[i])
						} else {
							var checkOther = true;

							angular.forEach($scope.existRoleData[i].role_permissions, function (item, key) {
								if (item.name == indItem) {
									item.status = 1;
								}
							});
							angular.forEach($scope.existRoleData[i].role_permissions, function (item, key) {
								if (item.status == 0) {
									checkOther = false;
								}
							});
							if (checkOther) {
								$scope.existRoleData[i].checkAll = 1;
							}
						}
					}
				}
			}

			$scope.createNewRole = function () {
				var createRolePermissions = [];
				angular.forEach($scope.existRoleData, function (value) {
					var tmpRole = {};
					tmpRole._id = value.id;
					tmpRole.name = value.name;
					var roleArray = []
					angular.forEach(value.role_permissions, function (roleValue) {
						if (roleValue.status === 1) {
							roleArray.push(roleValue.name);
						}
					});
					tmpRole.permissions = roleArray;
					createRolePermissions.push(tmpRole);
				});
				var secAction = "security-group_";
				var formData = {
					'name': $scope.createRoleName, 'clientId': '58e8c4da053e6839d40a26de',
					'roles': createRolePermissions, "createdBy": "Parag"
				};
				var params = { 'action': secAction, 'myparam': formData };
				console.log('sec data: ', params);
				httpServices.postRequest(API_URL, params).then(function (data) {
					console.log('team add : resp ', data);
					if (data.status == 200 && data.data != null) {
						$state.go('roleListing');
					} else {
						//show error msg
						$scope.errorMsgLogin = '';
					}
				}).catch(function (error) {
					console.log(error);
					$scope.errorMsgLogin = 'Server error.';
				});
			}

		})
		.controller('editRoleController', function ($scope, $location, localstorage, helperFactory, $state, httpServices) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			var tmpRole = JSON.parse(sessionStorage.currentRoleValue);
			console.log('session ----', tmpRole);
			if (typeof tmpRole.id == 'undefined' && tmpRole.id == '') {
				$state.go('roleListing');
				return false;
			}
			$scope.roleApproverView = tmpRole;

			$scope.$emit('pageTitle', "Edit Role");

//var action = API_PATH + "user_client_58dc93d67fc7b30d98e4d271";
			var action = API_PATH + "roles";
			//var action = API_PATH + "role_58da14d5053e682f848e64cc";
			var header = localstorage.get('authToken');
			//console.log(header)
			var params = { 'action': action, 'pageNo': 0, 'pageSize': 10, 'sortingOrder': 'ASC', 'fieldName': 'bajaj' };

			$scope.selectAll = [];

			$scope.existRoleData = tmpRole;


			$scope.checkAll = function (data, flag, key) {
				//console.log(typeof flag)
				//console.log($scope.existRoleData)

				if ($scope.existRoleData != null) {
					for (var i = 0; i < $scope.existRoleData.length; i++) {
						if (data == $scope.existRoleData[i].id) {
							//console.log($scope.existRoleData[i])
							if (flag) {
								angular.forEach($scope.existRoleData[i].role_permissions, function (item) {
									item.status = 1;
								});
								$scope.existRoleData[i].checkAll = 1;
							} else {
								angular.forEach($scope.existRoleData[i].role_permissions, function (item) {
									item.status = 0;
								});
								$scope.existRoleData[i].checkAll = 0;
							}
						}
					}
				}
			}

			$scope.checkIndCheck = function (key, flag, indItem) {
				//console.log(flag, indItem);
				for (var i = 0; i < $scope.existRoleData.length; i++) {

					if (key == $scope.existRoleData[i].id) {

						if (!flag) {
							angular.forEach($scope.existRoleData[i].role_permissions, function (item) {
								if (item.name == indItem) {
									item.status = 0;
								}
							});
							$scope.existRoleData[i].checkAll = 0;
							//console.log($scope.existRoleData[i])
						} else {
							var checkOther = true;

							angular.forEach($scope.existRoleData[i].role_permissions, function (item, key) {
								if (item.name == indItem) {
									item.status = 1;
								}
							});
							angular.forEach($scope.existRoleData[i].role_permissions, function (item, key) {
								if (item.status == 0) {
									checkOther = false;
								}
							});
							if (checkOther) {
								$scope.existRoleData[i].checkAll = 1;
							}
						}
					}
				}
			}

			/***httpServices.getRequest(API_URL, header, params).then(function (data) {
				if (data.status == 200 && data.data.length > 0) {
					$scope.roleData = data.data;
				} else {
					//show error msg
					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});**/


		})
		.controller('viewRoleController', function ($scope, $location, localstorage, helperFactory, $state, httpServices) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			var tmpRole = JSON.parse(sessionStorage.currentRoleValue);
			if (typeof tmpRole.id == 'undefined' && tmpRole.id == '') {
				$state.go('roleListing');
				return false;
			}

			$scope.$emit('pageTitle', "View Role");
			
			$scope.existRoleData = tmpRole;
			//var action = API_PATH + "user_client_58dc93d67fc7b30d98e4d271";
			var action = API_PATH + "roles";
			//var action = API_PATH + "role_58da14d5053e682f848e64cc";
			var header = localstorage.get('authToken');
			//console.log(header)
			var params = { 'action': action, 'pageNo': 0, 'pageSize': 10, 'sortingOrder': 'ASC', 'fieldName': 'bajaj' };
			$scope.editRoleView = function () {
			console.log('edit roleclicked----');
				$state.go('editRole');
			}
		})
		.controller('createTeamController', function ($scope, $location, localstorage, helperFactory, $state, httpServices, $timeout) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			$scope.$emit('pageTitle', "Create Team");
			//var action = API_PATH + "user_client_58dc93d67fc7b30d98e4d271";
			var action = API_PATH + "team";
			var header = localstorage.get('authToken');
			$scope.userInTeam = [];
			$scope.selectedUserIDs = [];
			//console.log(header)
			//var url = API_URL+"?action="+action+"&pageNo=0&pageSize=10&sortingOrder=ASC&fieldName=bajaj";
			//var myparam = {'pageNo':0,'pageSize':10,'sortingOrder':'ASC','fieldName':'bajaj'};
			var timeOut;
			$scope.addTeamUser = function (event, userName) {
				if (userName.length > 2) {
					$timeout.cancel(timeOut);
					timeOut = $timeout(function () {
						console.log('---', userName);
						var actionClientUser = API_PATH + "user_client_58da14d5053e682f848e64cc";
						var params = { 'action': actionClientUser };
						httpServices.getRequest(API_URL, header, params).then(function (data) {
							$scope.teamMembers = null;
							$scope.teamMembers = [];
							if (data.status == 200 && data.data.length > 0) {
								for (var i = 0; i < data.data.length; i++) {
									if ($scope.selectedUserIDs.indexOf(data.data[i].id) > -1) {
										//In the array!

									} else {
										//Not in the array
										$scope.teamMembers.push(data.data[i]);

									}
								}
							} else {
								//show error msg
								$scope.errorMsgLogin = '';
							}
						}).catch(function (error) {
							console.log(error);
							$scope.errorMsgLogin = 'Server error.';
						});

					}, 2000);
				} else {
					$scope.teamMembers = null;
				}
			}
			$scope.selectedUserInTeam = function (value) {
				$scope.userInTeam.push(value.firstName + ' ' + value.lastName);
				console.log(':2: ', $scope.userInTeam);
				$scope.selectedUserIDs.push(value.id);
				$scope.teamUserInput = null;
				$scope.teamMembers = null;
			}

			$scope.createNewTeam = function () {
				var formData = {
					'name': $scope.teamName, 'clientId': '58e86417053e6839d40a26d1', 'users': $scope.userInTeam,
					'createdBy': $scope.teamCreatedBy
				};

				var params = {
					'action': action, 'myparam': formData
				};
				httpServices.postRequest(API_URL, params).then(function (data) {
					console.log('team add : resp ', data);
					if (data.status == 200) {
						$state.go('teamListing');
					} else {
						//show error msg
						$scope.errorMsgLogin = '';
					}
				}).catch(function (error) {
					console.log(error);
					$scope.errorMsgLogin = 'Server error.';
				});
			}

		})
		.controller('userListingController', function ($scope, $location, localstorage, helperFactory, $state, httpServices) {

			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			$scope.$emit('pageTitle', "User Listing");
			//var action = API_PATH + "user_client_58dc93d67fc7b30d98e4d271";
			var action = API_PATH + "user_client_58da14d5053e682f848e64cc";
			var header = localstorage.get('authToken');
			//console.log(header)
			//var url = API_URL+"?action="+action+"&pageNo=0&pageSize=10&sortingOrder=ASC&fieldName=bajaj";
			//var myparam = {'pageNo':0,'pageSize':10,'sortingOrder':'ASC','fieldName':'bajaj'};
			var params = { 'action': action, 'pageNo': 0, 'pageSize': 10, 'sortingOrder': 'ASC', 'fieldName': 'bajaj' };
			httpServices.getRequest(API_URL, header, params).then(function (data) {
				console.log(data);
				
				if (data.status == 200 && data.data.length > 0) {
					$scope.userData = data.data;

				} else {
					//show error msg

					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});

		})
		.controller('addUserController', function ($scope, $http, $location, localstorage, helperFactory, $state, httpServices, $timeout) {
			if (!helperFactory.isLoggedIn()) {
				$state.go('login');
				return false;
			}
			$scope.files = [];
			$scope.$on("seletedFile", function (event, args) {
				$scope.$apply(function () {
					$scope.files.push(args.file);
				});
			});

			$scope.$emit('pageTitle', "Add User");
			var action = API_PATH + "user";
			//var action = API_PATH + "user_client_58dc93d67fc7b30d98e4d271";
			var header = localstorage.get('authToken');
			//console.log(header)
			//var url = API_URL+"?action="+action+"&pageNo=0&pageSize=10&sortingOrder=ASC&fieldName=bajaj";
			//var myparam = {'pageNo':0,'pageSize':10,'sortingOrder':'ASC','fieldName':'bajaj'};
			var paramsRole = { 'action': API_PATH + "roles" };

			httpServices.getRequest(API_URL, header, paramsRole).then(function (data) {
				console.log(data);
				if (data.status == 200 && data.data.length > 0) {
					$scope.existRoleData = data.data;

				} else {
					//show error msg
					$scope.errorMsgLogin = '';
				}
			}).catch(function (error) {
				console.log(error);
				$scope.errorMsgLogin = 'Server error.';
			});

			var timeOut;
			$scope.selectClientUser = function (event, clientName) {
				console.log('client name: ', clientName);
				if (clientName.length > 2) {
					$timeout.cancel(timeOut);
					timeOut = $timeout(function () {
						//console.log('---', clientName);
						var paramsClient = { 'action': API_PATH + "client_all" };
						httpServices.getRequest(API_URL, header, paramsClient).then(function (data) {
							//console.log(data);
							if (data.status == 200 && data.data.length > 0) {
								$scope.existingClient = data.data;
							} else {
								//show error msg
								$scope.errorMsgLogin = '';
							}
						}).catch(function (error) {
							//console.log(error);
							$scope.errorMsgLogin = 'Server error.';
						});


					}, 1000);
				} else {
					$scope.existingClient = null;
				}
			}

			$scope.selectedClientInUser = function (value) {
				$scope.clientUser = value;
				console.log(':2: ', $scope.clientUser);
				$scope.newUserClientName = value.clientName;
				$scope.existingClient = null;
			}

			$scope.createNewUser = function () {
				var modelData = $scope.user;
				var logo = $scope.userLogo;

				modelData.createdBy = "parag";
				modelData.clientId = "58e8c4da053e6839d40a26de";
				modelData.brandId = "58da334a053e684051592a45";
				modelData.securityGroups = [
					{
						"id": null,
						"name": "group",
						"clientId": "58e8c4da053e6839d40a26de",
						"createdBy": "saud",
						"roles": [
							{
								"name": "MANAGE_USER",
								"permissions": [
									"GET",
									"POST",
									"PUT",
									"DELETE"
								]
							}],
						"createdDate": 2152,
						"updatedDate": 11231
					}
				];
				modelData.profileImageName = '';
				modelData.hashedProfileImageName = '';
				modelData.userLogo = $scope.userLogo;
				/*var foxxxrmData = {
					"user": {
						"clientId": "58e738f6a8787e14db60931c",
						"brandId": "58da334a053e684051592a45",

						'firstName': $scope.newUserFirstName,
						'lastName': $scope.newUserLastName,
						'email': $scope.newUserEmail,
						'password': $scope.newUserPassword,
						'rePassword': $scope.newUserRePassword,
						'phone': $scope.newUserPhone,
						'logo': $scope.newUserProfileImg,
						'createdDate': 113113,
						'updatedDate': 415615,
						"createdBy": "saud",
						"securityGroups": [
							{
								"id": null,
								"name": "group",
								"clientId": "58da14d5053e682f848e64cc",
								"createdBy": "saud",
								"roles": [
									{
										"name": "MANAGE_USER",
										"permissions": [
											"GET",
											"POST",
											"PUT",
											"DELETE"
										]
									}],
								"createdDate": 2152,
								"updatedDate": 11231
							}
						],
					'profileImageName': 'dwqhdgywh',
					'hashedProfileImageName': 'dwqdvwqj',
				},
				"logo": ''
			};*/
				//console.log('___',JSON.stringify(modelData));
				console.log('___', logo);
				var params = {
					'action': action, 'rawBody': modelData, 'rawBodyKey': 'user',
					'fileKey': 'logo', 'file': ''
				};
				console.log('___', params);

				httpServices.postMediaRequest(API_URL + '/media', params).then(function (data) {
					console.log('user add : resp ', data);
					if (data.status == 200) {
						$state.go('userListing');
					} else {
						//show error msg
						$scope.errorMsgLogin = '';
					}
				}).catch(function (error) {
					console.log(error);
					$scope.errorMsgLogin = 'Server error.';
				});
			}


		}
		);
});