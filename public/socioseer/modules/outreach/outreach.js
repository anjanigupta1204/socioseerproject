'use strict';
define([
	'angular',
	'angularRoute'
], function(angular) { 
	angular.module('socioApp.outreach', ['ui.router'])
	.config(['$stateProvider', function($stateProvider) {
			$stateProvider.state('outreach', {
			url: '/outreach', 
			abstract: true,
			controller: 'outreachController',
			resolve: {
		            		activeMenu: function(){
		            			jQuery('body').removeClass('full-width');

		        		}
		    		}
		}).state("summary", { 
			url: '/summary',
	        templateUrl : 'modules/outreach/views/summary.html',
	        controller: 'summaryController',
    	}).state("createCampaign", { 
    		url: '/createCampaign',
	        templateUrl : 'modules/outreach/views/createCampaign.html'

    	}).state("composePost", { 
    		url: '/composePost',
	        templateUrl : 'modules/outreach/views/composePost.html',
	        controller: 'composePostController',
    	}).state("draft", { 
    		url: '/draft',
	        templateUrl : 'modules/outreach/views/draft.html',
	        controller: 'draftController',
    	}).state("scheduled", { 
    		url: '/scheduled',
	        templateUrl : 'modules/outreach/views/scheduled.html',
	        controller: 'scheduledController',
    	}).state("published", { 
    		url: '/published',
	        templateUrl : 'modules/outreach/views/published.html',
	        controller: 'publishedController',
    	}).state("tasks", { 
    		url: '/tasks',
	        templateUrl : 'modules/outreach/views/tasks.html',
	        controller: 'tasksController',
    	}).state("trending", { 
    		url: '/trending',
	        templateUrl : 'modules/outreach/views/trending.html',
	        controller: 'trendingController',
    	}).state("campaignListing", { 
    		url: '/campaignListing',
	        templateUrl : 'modules/outreach/views/campaignListing.html',
	        controller: 'campaignListingController',
    	}).state("viewCampaign", { 
    		url: '/viewCampaign',
	        templateUrl : 'modules/outreach/views/viewCampaign.html',
	        controller: 'viewCampaignController',
    	});
	}])
	.controller('outreachController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "OutReach"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('summaryController', function($scope,$location,localstorage,helperFactory,$state){
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 
		$scope.$emit('pageTitle', "Summary"); 
		jQuery('body').removeClass('full-width');
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		/*var nav = {	
			'audience':{'url':'#','iconClass':'fa fa-th-large'},
	  		'performance':{'url':'#','iconClass':'fa fa-th-large'},
	  		'settings':{'url':'#','iconClass':'fa fa-th-large'},
	  		'user management':{'url':'#','iconClass':'fa fa-th-large'}
	  	};
	  	angular.extend($scope.navItems, nav);*/
	}).controller('trendingController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Trending"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('tasksController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Tasks"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('tasksController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Tasks"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('publishedController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Published"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('scheduledController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Scheduled"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('draftController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Drafts"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('composePostController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Compose Post"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('campaignListingController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Campaign Listing"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	}).controller('viewCampaignController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "View Campaign"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	});
});