'use strict';
define([
	'angular',
	'angularRoute'
], function(angular) { 
	angular.module('socioApp.content', ['ui.router'])
	.config(['$stateProvider', function($stateProvider) {
			$stateProvider.state('suggestions', {
				url: '/suggestions',
				templateUrl: 'modules/content/views/suggestions.html',
				controller: 'contentController',
		}).state("contentLibrary", {
				url: '/contentLibrary',
		        templateUrl : 'modules/content/views/contentLibrary.html',
		        controller: 'contentLibraryController',
    	});
	}])
	.controller('contentController', function($scope,$location) {
		$scope.$emit('pageTitle', "Suggestions");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
	})
	.controller('contentLibraryController', function($scope,$location,localstorage,helperFactory,$state) {

		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}

		$scope.$emit('pageTitle', "Content Library");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
	});
});