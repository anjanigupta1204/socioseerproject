'use strict';
define([
	'angular',
	'angularRoute',
	'selectize',
	'datepicker'
], function(angular) { 
	angular.module('socioApp.client', ['ui.router'])
	.config(['$stateProvider', function($stateProvider) {
			$stateProvider.state('client', {
			url: '/client', 
			templateUrl : 'modules/client/views/client.html',
			controller: 'clientListController',
		}).state("addClient", { 
			url: '/addClient',
	        templateUrl : 'modules/client/views/addClient.html',
	        controller: 'addClientController',
    	}).state("createBrand", { 
			url: '/createBrand',
	        templateUrl : 'modules/client/views/createBrand.html',
	        controller: 'createBrandController',
    	})
	}]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
	}])
	.controller('addClientController', function($scope,$location,localstorage,helperFactory,$state,httpServices) {
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		$scope.editClient = false;
		$scope.$emit('pageTitle', "Add Client");  
		$scope.$emit('getHeader', true);  
		$scope.$emit('getSidebar', true);  
		$scope.$emit('getFooter', true);  
		$scope.$emit('titleSection', true); 
		jQuery(document).find('.simp-select').selectize();

		if(sessionStorage.currentCLient){
			$scope.editClient = sessionStorage.currentCLient;
			jQuery('#menu1').removeClass('active in');
			jQuery('.process-step.menu1').removeClass('active').addClass('complete');
			jQuery('.process-step.menu2').removeClass('disabled').addClass('active');
			jQuery('#menu2').addClass('active in');
		}
		//Progress bar form
 
 jQuery('.btn-circle').on('click',function(){
  jQuery('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
  jQuery(this).addClass('btn-info').removeClass('btn-default').blur();
 });

	jQuery('.next-step, .prev-step').on('click', function (e){
	var $activeTab = jQuery('.tab-pane.active');

		

   if ( jQuery(e.target).hasClass('next-step') )
   {
		var nextTab = $activeTab.next('.tab-pane').attr('id');
		jQuery('[href="#'+ nextTab +'"]').addClass('btn-info').removeClass('btn-default');
		jQuery('[href="#'+ nextTab +'"]').tab('show');
	  
		var res = nextTab.substring(4, 5);//res =1
		//alert("nextTab::"+nextTab);
		res = parseInt(res)-1;//res=2		
		var prevTab = "menu"+res+"";
		//alert("prevTab::"+prevTab);
		
		jQuery('.process-step.bs-wizard-step.'+nextTab+'').addClass('active').removeClass('disabled');
		jQuery('.process-step.bs-wizard-step.'+prevTab+'').addClass('complete').removeClass('active');
		
		
   }
   else
   {
		var prevTab = $activeTab.prev('.tab-pane').attr('id');
		jQuery('[href="#'+ prevTab +'"]').addClass('btn-info').removeClass('btn-default');
		jQuery('[href="#'+ prevTab +'"]').tab('show');
		
		var res = prevTab.substring(4, 5);//res =1
		//alert("prevTab::"+prevTab);
		res = parseInt(res)+1;//res=2		
		var nextTab = "menu"+res+"";
		//alert("nextTab::"+nextTab);
		
		jQuery('.process-step.bs-wizard-step.'+nextTab+'').addClass('disabled').removeClass('active');
		jQuery('.process-step.bs-wizard-step.'+prevTab+'').addClass('active').removeClass('complete');
   }
 });  
   var that = this;
	$scope.client ={};
	that.visibility = true; 
	/**
	* @saveClient step 1
	*/
    $scope.saveClientStep1 = function(){ 
					var formData = $scope.client; 
					if(sessionStorage.currentCLient){
						$scope.editClient = sessionStorage.currentCLient;
					}
					var startDateString = formData.startDateString;
					var endDateString = formData.endDateString; 
					$scope.competitiors = [];
					formData.status =  1; 
					angular.forEach(formData.competitiorsDefinitions, function(value) {
    					$scope.competitiors.push(value);
					});

					/*console.log($scope.competitiors);
					return false;*/
					formData.competitiorsDefinitions = $scope.competitiors;
					formData.updatedBy =  localstorage.get('userId'); 
					formData.subscriptionStartDate = helperFactory.convertToTimeStamp(startDateString);
					formData.subscriptionEndDate = helperFactory.convertToTimeStamp(endDateString); 
					//formData.clientLogo = $scope.myFile;
					 /* static data*/
					formData.createdBy = "Gaurav";
 					var headers = localstorage.get('authToken');  
					if(sessionStorage.currentCLient){
						formData.clientId = sessionStorage.currentCLient; 
					}
					 
					var action = "client";
					var params = {
							"action": action, "rawBody": formData, "rawBodyKey": "client",
							"fileKey": "", "file": ''
						}; 
						httpServices.postMediaRequest(API_URL + '/media', params).then(function (data) { 
							if (data.status == 200) {
								$scope.clientData = data.data;
					  			sessionStorage.currentCLient = $scope.clientData.id; 
							} else {
								//show error msg
								$scope.errorMsgLogin = '';
							}
						}).catch(function (error) {
							console.log(error);
							$scope.errorMsgLogin = 'Server error.';
						});
				  /*httpServices.postRequest(API_URL, userData , headers).then(function (data) { 
				  		if(data.status==200){ 
				  			$scope.clientData = data.data;
				  			sessionStorage.currentCLient = $scope.clientData.id;
				  			$scope.editClient =localstorage.get('editingClient');

				  		}else{
				  			//show error msg
				  			$scope.errorMsgLogin = 'Invalid credentials.';
				  		}
				  }).catch(function (error) {
				  	console.log(error);
					$scope.errorMsgLogin = 'Server error.';
				 });*/

    }
 	/**
 	* @SaveClient Step 2
 	*/
 	$scope.saveClientStep2 = function(){
 		var formData = $scope.clientInformation;  
 		if(sessionStorage.currentCLient){  
			formData.clientId = sessionStorage.currentCLient;
		} 
		console.log(formData);return false;
			var userData = {'action':API_PATH+'client/update','myparam':formData};
			var headers = localstorage.get('authToken'); 
			httpServices.postRequest(API_URL, userData , headers).then(function (data) { 
				if(data.status==200){ 
					$scope.clientData = data.data;
					sessionStorage.currentCLient = $scope.clientData.id;
					$scope.editClient =localstorage.get('editingClient');

				}else{
				//show error msg
					$scope.errorMsgLogin = 'Invalid credentials.';
				}
				}).catch(function (error) {
					console.log(error);
					$scope.errorMsgLogin = 'Server error.';
				});  
 	}
 	/**
 	* @saveClient Step 3
 	*/
 	$scope.saveClientStep3 = function(){
 		var formData = $scope.accountDetails;
 		formData.clientId =''; 
 		if(sessionStorage.currentCLient){ 
			formData.clientId = sessionStorage.currentCLient;
		} 
			var userData = {'action':API_PATH+'client/update','myparam':formData};
			var headers = localstorage.get('authToken'); 
			httpServices.postRequest(API_URL, userData , headers).then(function (data) { 
				if(data.status==200){ 
					$scope.clientData = data.data;
					sessionStorage.currentCLient = $scope.clientData.id;
					$scope.editClient =localstorage.get('editingClient');

				}else{
				//show error msg
					$scope.errorMsgLogin = 'Invalid credentials.';
				}
				}).catch(function (error) {
					console.log(error);
					$scope.errorMsgLogin = 'Server error.';
				});  
 	}

	}).controller('clientListController', function($scope,$stateParams,$location,localstorage,helperFactory,$state,httpServices){
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		sessionStorage.removeItem('currentCLient');
		$scope.$emit('pageTitle', "Clients");  
		$scope.$emit('getHeader', true);  
		$scope.$emit('getSidebar', true);  
		$scope.$emit('getFooter', true);  
		$scope.$emit('titleSection', true); 
		/** fetching Clients **/
		var action = API_PATH+"client_all";
		var header = localstorage.get('authToken');
		var params = {'action':action,'page':0,'size':10}; 
		httpServices.getRequest(API_URL,header,params).then(function (data) {
	  		if(data.status==200 && data.data.length>0){
				$scope.clientData = data.data; 
	  		}else{ 
	  			$scope.errorMsgLogin = '';
	  		}
		  }).catch(function (error) {
		  	console.log(error);
			$scope.errorMsgLogin = 'Server error.';
	    });
		  /* edit client*/
		  $scope.editClient = function(clientId){
				/*$state.transitionTo('.viewClient', {clientId: clientId}, {
			    location: true, 
			    relative: $state.$current, 
			    notify: false
			}); */
	
			 
		  }
	}).controller('createBrandController', function($scope,$location,localstorage,helperFactory,$state) {
		
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		} 

		$scope.$emit('pageTitle', "Create Brand"); 
		$scope.$emit('getHeader', true);
		$scope.$emit('getSidebar', true);
		$scope.$emit('getFooter', true);
		$scope.$emit('titleSection', true);  
		
	});
});