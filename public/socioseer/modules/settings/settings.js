'use strict';
define([
	'angular',
	'angularRoute'
], function(angular) { 
	angular.module('socioApp.settings', ['ui.router'])
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('setupMetrics', {
				url: '/setupMetrics',
				templateUrl: 'modules/settings/views/setupMetrics.html',
				controller: 'settingsController',
		}).state('setupReminders', {
				url: '/setupReminders',
				templateUrl: 'modules/settings/views/setupReminders.html',
				controller: 'settingsController',
		});
	}])
	.controller('settingsController', function($scope,$location,localstorage,helperFactory,$state) {
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		$scope.$emit('pageTitle', "Settings");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
		
	});
});