'use strict';
define([
	'angular',
	'angularRoute'
], function(angular) { 
	angular.module('socioApp.performance', ['ui.router'])
	.config(['$stateProvider', function($stateProvider) {
			$stateProvider.state('performance', {
			url: '/performance',
			templateUrl: 'modules/performance/views/summary.html',
			controller: 'performanceController',
		}).state("campaignReport", { 
			url: '/campaignReport',
	        templateUrl : 'modules/performance/views/campaignReport.html',
	        controller: 'campaignReportController',
    	}).state("teamReport", { 
    		url: '/teamReport',
	        templateUrl : 'modules/performance/views/teamReport.html',
	        controller: 'teamReportController',
    	});
	}])
	.controller('performanceController', function($scope,$location,localstorage,helperFactory,$state) {
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		$scope.$emit('pageTitle', "Performance");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
		
	}).controller('campaignReportController', function($scope,$location,localstorage,helperFactory,$state) {
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		$scope.$emit('pageTitle', "Campaign Report");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
		
	}).controller('teamReportController', function($scope,$location,localstorage,helperFactory,$state) {
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		$scope.$emit('pageTitle', "Team Report");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
		
	});
});