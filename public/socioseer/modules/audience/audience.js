'use strict';
define([
	'angular',
	'angularRoute'
], function(angular) { 
	angular.module('socioApp.audience', ['ui.router'])
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('createAudience', {
				url: '/createAudience',
				templateUrl: 'modules/audience/views/createAudience.html',
				controller: 'audienceController',
		}).state("viewAudience", { 
				url: '/viewAudience',
		        templateUrl : 'modules/audience/views/viewAudience.html',
		        controller: 'viewAudienceController',
    	});
	}])
	.controller('audienceController', function($scope,$location) {
		$scope.$emit('pageTitle', "Create Audience");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
		$scope.createAudience = function () {

		}
	})
	.controller('viewAudienceController', function($scope,$location,localstorage,helperFactory,$state) {
		if(!helperFactory.isLoggedIn()){
			$state.go('login');
			return false;
		}
		
		$scope.$emit('pageTitle', "View Audience");
		$scope.$emit('getHeader', true);
		$scope.$emit('getFooter', true);
	});
});







 