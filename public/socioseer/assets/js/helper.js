/**
* Helper Methods
* @access global
* @author Gaurav Sirauthiya
* @since March 25, 2017
* @version 1.0.0
*/
define([
	'angular',
	'angularRoute'
], function (angular, angularRoute) {
	angular.module('socioApp.helperFactory', [])
		.factory('helperFactory', function (localstorage, $location, httpServices) {
			var helper = {};
			/**
			* @check user logged in
			*/
			helper.isLoggedIn = function () {
				return true;
				if (typeof localstorage.get('logged_in') != 'undefined' && localstorage.get('logged_in') != '' && localstorage.get('logged_in') != 'false') {
					return true;
				}
			}
			/**
			* @check user select a team
			*/
			helper.checkKeysExists = function (key) {
				if (typeof localstorage.get(key) != 'undefined' && localstorage.get(key) != '' && localstorage.get(key) != 'false') {
					return true;
				}
			}

			/**
			* @convert date to time Stamp
			*/
			helper.convertToTimeStamp = function (dateString) {
				if (typeof dateString != 'undefined') {
					var y = dateString.split('/');
					var u = y[1] + '/' + y[0] + '/' + y[2];
					return Date.parse(u);
				}
			}

			return helper;
		}).filter('myFormat', function () {
			return function (x) {
				return x.toUpperCase();
			}
		});

});
