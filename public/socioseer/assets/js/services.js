/**
	@Name: httpServices
	@Type: Services
	@Author: Anjani Kr Gupta
	@Date: 23rd-Mar-2017
*/
 
'use strict';
define([
	'angular',
	'angularRoute'
], function(angular,angularRoute) {
	 
	angular.module('socioApp.services', [])
	.service('httpServices', function ($http, $q) {
	var httpService = {};
	httpService.postRequest = function(_url, _data,_headers=false){
		if(_headers){
			var headerSent = {'Content-Type' : 'application/json','X-AUTH-HEADER' : _headers};
		}else{
			var headerSent = {'Content-Type' : 'application/json'};
		}
		var deferred = $q.defer(); 
		$http({
			method: 'POST',
			url: _url,
			data: _data,
			headers: headerSent,
			cache: false,
		}).success(function(data, status, headers, config){
			deferred.resolve(data);
		})
		.error(function(data, status, headers, config){
			deferred.reject(data);
		});
		return deferred.promise;
	}

	httpService.postMediaRequest = function(_url, _data,_headers=false){
		var deferred = $q.defer(); 
		$http({
			method: 'POST',
			url: _url,
			data: _data,
			cache: false,
		}).success(function(data, status, headers, config){
			deferred.resolve(data);
		})
		.error(function(data, status, headers, config){
			deferred.reject(data);
		});
		return deferred.promise;
	}

	httpService.getRequest = function(_url,authHeader,params=false){ 
		var deferred = $q.defer();
		$http({
			method: 'GET',
			url: _url,
			params: params,
			headers: {
                'Content-Type': 'application/json',
                'X-AUTH-HEADER': authHeader
            },
			cache: false,

		}).success(function(data, status, headers, config){
			deferred.resolve(data);
		})
		.error(function(data, status, headers, config){
			deferred.reject(data);
		});
		return deferred.promise;
	}
	
  	return httpService;
	});
});




 