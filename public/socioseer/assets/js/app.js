/**
@Name: socioApp
@Type: Main module
@author: Anjani Kr Gupta
@contributor Gaurav Sirauthiya
@since: 23rd-Mar-2017
@version 1.0.0
*/
'use strict';  
define([
	'angular',
	'angularRoute',
	'assets/js/constant',
	'helper',
	'jquery',
	'bootstrap',
	'mentis',
	'pace',  
	'script',
	'modules/user/user', 
	'modules/audience/audience',
	'modules/content/content',
	'modules/outreach/outreach',
	'modules/client/client',
	'modules/performance/performance',
	'modules/settings/settings',
	'assets/js/services',
	'assets/js/storage'
], function(angular, angularRoute) { 
	return angular.module('socioApp', [
		'ui.router',
		'720kb.datepicker',
		'socioApp.services',
		'socioApp.helperFactory',
		'socioApp.localStorageFactory',
		'socioApp.user',  
		'socioApp.audience',
		'socioApp.content',
		'socioApp.outreach',
		'socioApp.client',
		'socioApp.performance',
		'socioApp.settings'
	])
	.config(['$locationProvider','$stateProvider','$urlRouterProvider', function($locationProvider, $stateProvider, $urlRouterProvider) {
		//$locationProvider.hashPrefix('!'); 
		$urlRouterProvider.otherwise('/login');

	}])
	.controller('mainController', function($scope,helperFactory,localstorage,$state,$window){

	  	$scope.metisMenu = function() {
		    $('#side-menu').metisMenu(); 
			
	  	}

	  	$scope.init = function(){ 
	  	}

	  	/*$scope.navItems = {
	  		'outreach':{'url':'#','iconClass':'fa fa-th-large','parent':0},
	  		'summary':{'url':'#','iconClass':'fa fa-th-large','parent':'outreach'},
	  		'client':{'url':'#','iconClass':'fa fa-th-large'},
	  		'content':{'url':'#','iconClass':'fa fa-th-large'},
	  	
	  	};*/

	  	$scope.minimizeMenu = function(){
        	$("body").toggleClass("mini-navbar");
	  	}
	  	$scope.logout = function(){
	  		localstorage.set('logged_in',false);
	  		$window.localStorage.clear();
			$state.go('login');
	  	}
		$scope.imageUrl = BASE_URL + 'assets/images/';
 		$scope.header = 'includes/header.html';
 		$scope.sidebar = 'includes/sidebar.html';
 		$scope.footer = 'includes/footer.html';
 		$scope.titlesection = 'includes/title-section.html';
		$scope.title = 'SocioSeer';
		$scope.includeHeader  = true;
		$scope.includeSidebar  = true;
		$scope.includeFooter  = true;
		$scope.$on('pageTitle', function(event, data) { 
				$scope.title = data;
		 });
		$scope.$on('getHeader', function(event, data) { 
			$scope.includeHeader = data; 
			if($scope.includeHeader && $scope.includeHeader == true){
				$scope.header = 'includes/header.html';
			}else{
				$scope.header = '';
			}
		 });
	 	$scope.$on('getSidebar', function(event, data) { 
			$scope.includeSidebar = data; 
			if($scope.includeSidebar && $scope.includeSidebar == true){
				$scope.sidebar = 'includes/sidebar.html';
			}else{
				$scope.sidebar = '';
			}
		 });
	 	$scope.$on('titleSection', function(event, data) { 
			$scope.titlesection = data; 
			if($scope.titlesection && $scope.titlesection == true){
				$scope.titlesection = 'includes/title-section.html';
			}else{
				$scope.titlesection = '';
			}
		 });
	 	$scope.$on('getFooter', function(event, data) { 
			$scope.includeFooter = data; 
			if($scope.includeFooter && $scope.includeFooter == true){
				$scope.footer = 'includes/footer.html';
			}else{
				$scope.footer = '';
			}
		 });

	});
});