/**
* Storage JS
* @author Gaurav Sirauthiya <sirauthiya.gaurav@orangemantra.in>
* @since March,25 2017
* @version 1.0.0
*/

 /**
* Local Storage Methods
* @access global
* @author Gaurav Sirauthiya
* @since March 25, 2017
* @version 1.0.0
*/
define([	
	'angular',
	'angularRoute'
], function(angular,angularRoute){
	angular.module('socioApp.localStorageFactory',[])
	.factory('localstorage',function($window){
		return {
			set: function(key, value) {
			  $window.localStorage[key] = value;
			},
			get: function(key, defaultValue) {
			  return $window.localStorage[key] || defaultValue;
			},
			setObject: function(key, value) {
			  $window.localStorage[key] = JSON.stringify(value);
			},
			getObject: function(key) {
			  return JSON.parse($window.localStorage[key] || '{}');
			}
		  }
	});
	
});